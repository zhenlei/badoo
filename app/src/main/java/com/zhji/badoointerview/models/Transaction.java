package com.zhji.badoointerview.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by zhenlei on 17/04/16.
 */
public class Transaction implements Parcelable {
    @SerializedName("amount")
    float amount;

    @SerializedName("sku")
    String sku;

    @SerializedName("currency")
    String currency;

    public float getAmount() {
        return amount;
    }

    public void setAmount(float amount) {
        this.amount = amount;
    }

    public String getSku() {
        return sku;
    }

    public void setSku(String sku) {
        this.sku = sku;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    @Override
    public String toString() {
        return String.format("%s: %f %s", sku, amount, currency);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeFloat(this.amount);
        dest.writeString(this.sku);
        dest.writeString(this.currency);
    }

    public Transaction() {
    }

    protected Transaction(Parcel in) {
        this.amount = in.readFloat();
        this.sku = in.readString();
        this.currency = in.readString();
    }

    public static final Parcelable.Creator<Transaction> CREATOR = new Parcelable.Creator<Transaction>() {
        @Override
        public Transaction createFromParcel(Parcel source) {
            return new Transaction(source);
        }

        @Override
        public Transaction[] newArray(int size) {
            return new Transaction[size];
        }
    };
}
