package com.zhji.badoointerview.utils;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.zhji.badoointerview.BadooApp;
import com.zhji.badoointerview.models.Rate;
import com.zhji.badoointerview.models.Transaction;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.List;

import rx.Observable;
import rx.Subscriber;
import rx.schedulers.Schedulers;

/**
 * Created by zhenlei on 17/04/16.
 */
public class LoadFileUtils {
    public static Observable<List<Rate>> readRateFile(String file) {
        return Observable.create(new Observable.OnSubscribe<List<Rate>>() {
            @Override
            public void call(Subscriber<? super List<Rate>> subscriber) {
                BufferedReader reader = null;
                StringBuilder text = new StringBuilder();
                try {
                    reader = new BufferedReader(new InputStreamReader(BadooApp.getContext().getAssets().open(file)));

                    String line;
                    while ((line = reader.readLine()) != null) {
                        text.append(line);
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                    subscriber.onError(e);
                    return;
                } finally {
                    if (reader != null) {
                        try {
                            reader.close();
                        } catch (IOException e) {
                            e.printStackTrace();
                            subscriber.onError(e);
                            return;
                        }
                    }
                }
                List<Rate> rates = new Gson().fromJson(text.toString().replaceAll("\\s+", " "), new TypeToken<List<Rate>>() {
                }.getType());
                subscriber.onNext(rates);
                subscriber.onCompleted();
            }
        }).subscribeOn(Schedulers.io());
    }

    public static Observable<List<Transaction>> readTransactionFile(String file) {
        return Observable.create(new Observable.OnSubscribe<List<Transaction>>() {
            @Override
            public void call(Subscriber<? super List<Transaction>> subscriber) {
                BufferedReader reader = null;
                StringBuilder text = new StringBuilder();
                try {
                    reader = new BufferedReader(new InputStreamReader(BadooApp.getContext().getAssets().open(file)));

                    String line;
                    while ((line = reader.readLine()) != null) {
                        text.append(line);
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                    subscriber.onError(e);
                    return;
                } finally {
                    if (reader != null) {
                        try {
                            reader.close();
                        } catch (IOException e) {
                            e.printStackTrace();
                            subscriber.onError(e);
                            return;
                        }
                    }
                }
                List<Transaction> transactions = new Gson().fromJson(text.toString().replaceAll("\\s+", " "), new TypeToken<List<Transaction>>() {
                }.getType());
                subscriber.onNext(transactions);
                subscriber.onCompleted();
            }
        }).subscribeOn(Schedulers.io());
    }
}
