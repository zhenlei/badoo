package com.zhji.badoointerview.utils;

import android.util.Log;

import com.zhji.badoointerview.models.Rate;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;

/**
 * Created by zhenlei on 17/04/16.
 */
public class Conversion {
    private static final String TAG = Conversion.class.getSimpleName();
    private static final String CURRENT_CONVERSION = "GBP";
    private static Conversion sConversion;
    private HashMap<String, Float> mConversionMap;

    private Conversion() {
        mConversionMap = new HashMap();
    }

    public static Conversion getInstance() {
        if (sConversion == null) {
            sConversion = new Conversion();
        }
        return sConversion;
    }

    public void processRate(List<Rate> rates) {
        HashMap<String, ArrayList<Rate>> neighborsCache = new HashMap();

        Set<String> allMoneys = new HashSet();

        for (Rate rate : rates) {
            String from = rate.getFrom();
            String to = rate.getTo();

            allMoneys.add(from);
            allMoneys.add(to);

            mConversionMap.put(getKey(from, to), rate.getRate());

            if (neighborsCache.containsKey(from)) {
                neighborsCache.get(from).add(rate);
            } else {
                ArrayList list = new ArrayList();
                list.add(rate);
                neighborsCache.put(from, list);
            }
        }

        allMoneys.remove(CURRENT_CONVERSION);
        mConversionMap.put(getKey(CURRENT_CONVERSION, CURRENT_CONVERSION), 1f);
        for (String money : allMoneys) {

            // BFS + PD
            LinkedList<Rate> queues = new LinkedList();
            Rate node = new Rate(money, 1, money);
            queues.addLast(node);
            mConversionMap.put(getKey(money, money), 1f);
            HashMap<String, Boolean> visited = new HashMap();
            visited.put(node.getTo(), true);
            while (!queues.isEmpty()) {
                node = queues.removeFirst();
                List<Rate> neighbors = neighborsCache.get(node.getTo());
                for (Rate neighbor : neighbors) {
                    String to = neighbor.getTo();
                    if (!visited.containsKey(to)) {
                        visited.put(to, true);
                        queues.addLast(neighbor);
                        mConversionMap.put(getKey(money, neighbor.getTo()),
                                mConversionMap.get(getKey(money, neighbor.getFrom())) * neighbor.getRate());
                    }
                }
            }
        }
    }

    private String getKey(String from, String to) {
        return String.format("%s:%s", from, to);
    }

    /**
     * Check if has conversion to GBP
     *
     * @param currency could be GBP, USD, EUR or etc
     * @return true there is, otherwise there is not.
     */
    public boolean hasConversion(String currency) {
        return mConversionMap.containsKey(getKey(currency, CURRENT_CONVERSION));
    }

    /**
     * Convert amount to GBP
     *
     * @param amount   amount of money
     * @param currency could be GBP, USD, EUR or etc
     * @return if don't has conversion, return 0
     */
    public float toGBP(float amount, String currency) {
        return hasConversion(currency) ? amount * mConversionMap.get(getKey(currency, CURRENT_CONVERSION)) : 0;
    }

}
