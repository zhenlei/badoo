package com.zhji.badoointerview.activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.Toast;

import com.zhji.badoointerview.R;
import com.zhji.badoointerview.adapter.ProductsAdapter;
import com.zhji.badoointerview.models.Transaction;
import com.zhji.badoointerview.utils.LoadFileUtils;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;

public class ProductsActivity extends AppCompatActivity {
    private static final String TAG = ProductsActivity.class.getSimpleName();

    @Bind(R.id.rv_products)
    RecyclerView mRecyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_products);
        ButterKnife.bind(this);
        getSupportActionBar().setTitle(getString(R.string.ab_product));


        final List<Transaction> allTransactions = new ArrayList();
        Observable.just("transactions1.json", "transactions2.json")
                .flatMap(name -> LoadFileUtils.readTransactionFile(name))
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(transactions -> allTransactions.addAll(transactions),
                        throwable -> Toast.makeText(this, "Failed to load transactions.", Toast.LENGTH_SHORT).show(),
                        () -> loadRecycleView(allTransactions));
    }

    private void loadRecycleView(List<Transaction> transactions) {
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
        mRecyclerView.setAdapter(new ProductsAdapter(this, transactions));
    }
}
