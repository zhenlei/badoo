package com.zhji.badoointerview.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.MenuItem;
import android.widget.TextView;

import com.zhji.badoointerview.R;
import com.zhji.badoointerview.adapter.TransactionsAdapter;
import com.zhji.badoointerview.models.Rate;
import com.zhji.badoointerview.models.Transaction;
import com.zhji.badoointerview.utils.Conversion;
import com.zhji.badoointerview.utils.LoadFileUtils;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;

public class TransactionsActivity extends AppCompatActivity {
    private static final String TAG = TransactionsActivity.class.getSimpleName();
    public static final String EXTRA_SKU = "com.zhji.badoointerview.EXTRA_SKU";
    public static final String EXTRA_TRANSACTIONS = "com.zhji.badoointerview.EXTRA_TRANSACTIONS";

    @Bind(R.id.tv_total)
    TextView mTotalTextView;

    @Bind(R.id.rv_transactions)
    RecyclerView mRecyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_transactions);
        ButterKnife.bind(this);

        final Bundle extras = getIntent().getExtras();
        final String sku = extras.getString(EXTRA_SKU);
        final ArrayList<Transaction> transactions = extras.getParcelableArrayList(EXTRA_TRANSACTIONS);

        getSupportActionBar().setTitle(getString(R.string.ab_transactions, sku));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        List<Rate> allRates = new ArrayList();
        Observable.just("rates1.json", "rates2.json")
                .flatMap(name -> LoadFileUtils.readRateFile(name))
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(rates -> allRates.addAll(rates),
                        throwable -> Log.e(TAG, throwable.toString()),
                        () -> loadRecycleView(transactions, allRates));
    }

    private void loadRecycleView(List<Transaction> transactions, List<Rate> rates) {
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
        mRecyclerView.setAdapter(new TransactionsAdapter(this, transactions, rates));

        float total = 0;
        Conversion conversion = Conversion.getInstance();
        conversion.processRate(rates);
        for (Transaction transaction : transactions) {
            if (conversion.hasConversion(transaction.getCurrency())) {
                total += conversion.toGBP(transaction.getAmount(), transaction.getCurrency());
            }
        }
        mTotalTextView.setText(getString(R.string.amount_total, String.format("%.2f", total)));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}
