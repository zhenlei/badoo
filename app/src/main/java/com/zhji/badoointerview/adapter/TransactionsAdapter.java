package com.zhji.badoointerview.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.zhji.badoointerview.R;
import com.zhji.badoointerview.activity.TransactionsActivity;
import com.zhji.badoointerview.models.Rate;
import com.zhji.badoointerview.models.Transaction;
import com.zhji.badoointerview.utils.Conversion;
import com.zhji.badoointerview.utils.LoadFileUtils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;

/**
 * Created by zhenlei on 17/04/16.
 */
public class TransactionsAdapter extends RecyclerView.Adapter<TransactionsAdapter.ViewHolder> {
    private final Context mContext;
    private final List<Rate> mRates;
    List<Transaction> mTransactions;

    public TransactionsAdapter(Context context, List<Transaction> transactions, List<Rate> rates) {
        mContext = context;
        mTransactions = transactions;
        mRates = rates;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.row_transactions, parent, false));
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Transaction transaction = mTransactions.get(position);
        float amount = transaction.getAmount();
        String currency = transaction.getCurrency();
        holder.amountView.setText(mContext.getString(R.string.amount, String.format("%.2f", amount), currency));
        Conversion conversion = Conversion.getInstance();
        conversion.processRate(mRates);
        if (conversion.hasConversion(currency)) {
            holder.convertView.setText(mContext.getString(R.string.amount_GBP, String.format("%.2f", conversion.toGBP(amount, currency))));
        } else {
            holder.convertView.setText(R.string.text_not_converted);
        }
    }


    @Override
    public int getItemCount() {
        return mTransactions.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @Bind(R.id.tv_title)
        TextView amountView;

        @Bind(R.id.tv_subtitle)
        TextView convertView;

        public ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }
}
