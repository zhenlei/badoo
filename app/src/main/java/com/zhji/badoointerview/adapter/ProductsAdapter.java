package com.zhji.badoointerview.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.zhji.badoointerview.BadooApp;
import com.zhji.badoointerview.R;
import com.zhji.badoointerview.activity.TransactionsActivity;
import com.zhji.badoointerview.models.Transaction;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import rx.Observable;

/**
 * Created by zhenlei on 17/04/16.
 */
public class ProductsAdapter extends RecyclerView.Adapter<ProductsAdapter.ViewHolder> {
    private final Context mContext;
    HashMap<String, Integer> mTransactionsMap;
    List<Transaction> mTransactions;
    List<String> mSkuList;

    public ProductsAdapter(Context context, List<Transaction> transactions) {
        mContext = context;
        mTransactionsMap = new HashMap();
        mSkuList = new ArrayList();

        String sku;
        for (Transaction transaction : transactions) {
            sku = transaction.getSku();
            if (mTransactionsMap.containsKey(sku)) {
                mTransactionsMap.put(sku, mTransactionsMap.get(sku) + 1);
            } else {
                mTransactionsMap.put(sku, 1);
                mSkuList.add(sku);
            }
        }
        Collections.sort(mSkuList);
        mTransactions = transactions;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.row_products, parent, false));
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        String sku = mSkuList.get(position);
        holder.skuView.setText(sku);
        int number = mTransactionsMap.get(sku);
        holder.transactionsView.setText(
                mContext.getString(
                        number == 1 ? R.string.transaction_singular : R.string.transaction_plural,
                        number));

        holder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext, TransactionsActivity.class);
                intent.putExtra(TransactionsActivity.EXTRA_SKU, sku);
                List<Transaction> transactions = Observable.from(mTransactions)
                        .filter(transaction -> transaction.getSku().equals(sku))
                        .toList()
                        .toBlocking()
                        .single();
                intent.putParcelableArrayListExtra(TransactionsActivity.EXTRA_TRANSACTIONS, new ArrayList(transactions));
                mContext.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mSkuList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @Bind(R.id.ll_product)
        View backgroundView;

        @Bind(R.id.tv_title)
        TextView skuView;

        @Bind(R.id.tv_subtitle)
        TextView transactionsView;

        public ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }

        public void setOnClickListener(View.OnClickListener listener) {
            backgroundView.setOnClickListener(listener);
        }
    }
}
