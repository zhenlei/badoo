package com.zhji.badoointerview;

import android.app.Application;
import android.content.Context;

/**
 * Created by zhenlei on 17/04/16.
 */
public class BadooApp extends Application {
    private static Context sContext;

    public static Context getContext() {
        return sContext;
    }

    public static void setContext(final Context context) {
        sContext = context;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        setContext(this);
    }
}
